# Estruturas de dados {#sec:estruturas}

## Sale


```c
struct sale {
    Produto produto;
    double preco;
    int unidades;
    char modalidade;
    Cliente cliente;
    int mes;
    int filial;
};
```

A presente estrutura é utilizada como meio de validação de linhas de venda.
À medida que são lidas linhas, os dados são introduzidos na presente estrutura e
posteriormente validados. Assim, a estrutura é meramente utilizada como meio de
validação de dados.

## Cliente


```c
struct cliente {
    char* codigo;
    Bool usado[NUM_FILIAIS];
};
```

Esta estrutura possui uma _string_ com o código de cliente assim como um _array_
com o tamanho definido na macro NUM_FILIAIS que permite saber de forma
instantânea se o cliente efetuou compras numa dada filial.


## Produto


```c
struct produto {
    char* codigo;
    Bool usado[NUM_FILIAIS];
};
```

Esta estrutura possui uma _string_ com o código de produto assim como um _array_
com o tamanho definido na macro NUM_FILIAIS que permite saber de forma
instantânea se o produto foi ou não transacionado numa dada filial.


## Catálogo de Clientes


```c
struct cat_clientes {
    int total;
    Avl lista[26][5][10];
    int usados;
};
```

Esta estrutura possui dois inteiros: um deles permite saber o número total de
clientes presentes no catálogo e o segundo permite saber de forma imediata o
número de clientes que realizaram compras em qualquer uma das filiais. A
presente estrutura conta ainda com uma _hashtable_ de AVL (Árvore Binária de
Procura Balanceada) onde estão os clientes que partilham os seguintes aspetos:

  * Os códigos de cliente começam pela mesma letra
  * O primeiro dígito do primeiro código cliente coincide com o primeiro dígito do
    segundo código de cliente
  * O segundo dígito do primeiro código de cliente coincide com o segundo dígito
    do segundo código de cliente

Esta organização de dados permite verificar se um cliente consta ou não do
catálogo de clientes de forma praticamente instantânea.

O catálogo de clientes possui ainda uma outra estrutura que permite organizar
códigos de cliente numa lista por forma a facilitar a resposta a determinadas
queries.


```c
struct lista_clientes {
    ArrayList codigos;
};
```


## Catálogo de Produtos


```c
struct cat_produtos {
    int total;
    Avl lista[26][26][9][10];
    int usados;
};
```

Esta estrutura possui dois inteiros: um deles permite saber o número total de
produtos presentes no catálogo e o segundo permite saber de forma imediata o
número de produtos que foram transacionados, independentemente da filial em que
isso aconteceu.
A presente estrutura conta ainda com uma _hashtable_ de AVL onde estão os
produtos que partilham os seguintes aspetos:

  * Os códigos de produto começam pela mesma letra
  * A segunda letra do primeiro código é a mesma que a segunda letra do segundo
    código
  * O primeiro dígito do código coincide com o primeiro dígito do segundo código
  * O segundo dígito do código coincide com o segundo dígito do segundo código

O catálogo de produtos possui ainda uma outra estrutura que permite organizar
códigos de produto numa lista por forma a facilitar a resposta a determinadas
_queries_.


```c
struct lista_produtos {
    ArrayList codigos;
};
```


## Faturação


```c
struct facprod {
    Produto produto;
    double totalFacturadoN;
    double totalFacturadoP;
    int unidadesN;
    int unidadesP;
    int registosVendaN;
    int registosVendaP;
};
```
Esta estrutura possui quatro inteiros: um deles permite saber o número de
unidades vendidas de um produto em regime normal, outro permite saber o número
de unidades vendidas em regime promocional. Já os restantes inteiros têm o mesmo
fim dos previamente mencionados mas referem-se ao número de registos de vendas de
um dado produto em ambos os regimes.
A presente estrutura conta ainda com dois `double`'s: um deles permite saber o
total facturado com um produto em regime normal e o outro em regime promocional.
Possui também uma estrutura Produto referida anteriormente.


```c
struct facanual {
    Avl produtos[26][26][9][10];
};
```

Esta estrutura é constituída unicamente por uma _hashtable_ de AVL. Em cada AVL
encontram-se estruturas `FacProd` de produtos que possuem as seguintes
caraterísticas em comum:

  * Os códigos de produto começam pela mesma letra
  * A segunda letra do primeiro código é a mesma que a segunda letra do segundo
    código
  * O primeiro dígito do código coincide com o primeiro dígito do segundo código
  * O segundo dígito do código coincide com o segundo dígito do segundo código


```c
struct facmensal {
    double totalFacturado;
    int totalVendas;
    Avl produtos[26][26][9][10];
};
```


A presente estrutura contém um `double` que permite imediatamente saber o total
faturado num mês, um inteiro que permite saber o total de transações efetuadas
num mês.
A faturação mensal contém ainda uma estrutura do tipo da mencionada acima.


```c
struct factotal {
    FacMensal factFilialMensal[NUM_FILIAIS][12];
    FacAnual factFilialAnual[NUM_FILIAIS];
};
```


A faturação total contém uma estrutura referente à faturação mensal e outra
referente à faturação anual. A faturação mensal está organizada por filiais
através da macro NUM_FILIAIS e por meses. A faturação anual está organizada por
filiais através da macro NUM_FILIAIS.


## Filial


```c
struct infoprod {
    Produto p;
    int quantidadeN;
    int quantidadeP;
    float facturacaoN;
    float facturacaoP;
};
```


Esta é a estrutura que contém a informação relativa a um produto comprado por um
cliente. Contém dois inteiros: o primeiro indica a quantidade de unidades
compradas em regime normal e o segundo indica a quantidade de unidades compradas
em regime promocional. Conta ainda com dois `float`, sendo que o primeiro contém
a faturação gerada pelas unidades compradas em regime normal e o outro a
faturação gerada pelas unidades compradas em regime promocional.


```c
struct filialanual {
    Avl clientes[26][5][10];
}
```

Esta estrutura contém informação relativa aos produtos que cada cliente comprou
durante um ano. A estrutura conta com uma _hashtable_ de AVL.


```c
struct clienteprods {
    Cliente c;
    ArrayList produtos;
};
```

A presente estrutura associa a cada cliente os produtos que ele comprou ao longo
de uma ano ou mês, consoante a outra estrutura a que está associada. Contém uma
estrutura do tipo `Cliente` e um `ArrayList` de produtos.


```c
struct filialmensal {
    Avl clientes[26][5][10];
};
```

Esta estrutura contém informação relativa aos produtos que cada cliente comprou
durante um mês. A estrutura conta com uma _hashtable_ de AVL (Árvore Binária de
Procura Automaticamente Balanceada).


```c
struct filial {
    FilialMensal meses[12];
    FilialAnual anual;
};
```

Esta estrutura contém um _array_ de estruturas do tipo `FilialMensal` e uma
estrutura do tipo `FilialAnual`. As primeiras estão organizadas num _array_ cujo
tamanho é 12, uma vez que existem 12 meses no ano e cada _slot_ do _array_
contém uma estrutura do tipo `FilialMensal` que permite saber que produtos cada
cliente comprou nesse mês.


## Navegador


```c
struct strings {
    int total;
    char **arrayStrings;
    int pag;
    int indice;
};
```

A presente estrutura é utilizada para guardar todas as _strings_ que possam ser
mostradas pelo navegador. A estrutura é constituída por um inteiro que guarda
o número total de _strings_ armazenadas, um _array_ de _strings_ que guardas as
_strings_ a ser mostradas, um inteiro que guarda a página do navegador em que o
utilizador se encontra e um índice que guarda em posição do _array_ de _strings_
o navegador se encontra.


```c
struct pagina {
    int total;
    char **arrayStrings;
    int indice;
};
```

A estrutura que representa uma página do navegador contém um representa o total
de _strings_ numa página, um _array_ de _strings_ que contém as _strings_ a serem
mostradas e o índice que representa a posição em que o utilizador se encontra
no _array_ de _strings_.


## SGV


```c
struct sgv {
    CatClientes clientes;
    CatProdutos produtos;
    FacTotal faturacao;
    Filial* filiais;
    int vendasValidas;
};
```

A estrutura que representa o Sistema de Gestão de Vendas contém um Catálogo de
Clientes (`CatClientes`), um Catálogo de Produtos (`CatProdutos`), a faturação
(`FacTotal`), um _array_ de Filiais (`Filial`) e um inteiro que contém o número
de vendas válidas.

