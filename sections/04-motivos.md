# Motivo de escolha de estruturas {#sec:motivos}

## Cliente

Optamos por ter um _array_ de `bool` porque torna bastante mais simples a
resposta às _queries_ 5 e 6, porque para cada cliente é imediato verificar se
fez ou não compras em todas as filiais ou se fez compras em alguma filial.

## Produto

Optamos por ter um _array_ de `bool` porque torna bastante mais simples a
resposta às queries 4 e 6, porque para produto é imediato verificar se foi ou
não comprado em todas as filiais ou se foi comprado em alguma filial.

## Catálogo de Clientes

![Diagrama explicativo do Catálogo de Clientes](figures/catalogo_clientes.pdf){ height=10cm }

Optamos por ter uma _hashtable_ de AVL para diminuir o número de elementos da
AVL em que procuramos por um determinado cliente, uma vez que existem no
máximo 100 clientes na AVL de procura. Esta estrutura torna também a inserção
de novos clientes muito rápida.

## Catálogo de Produtos

![Diagrama explicativo do Catálogo de Produtos](figures/catalogo_produtos.pdf){ height=10cm }

Nesta estrutura de dados a justificação é em tudo análoga à da estrutura
anterior. Consideramos, a determinada altura, o endereçamento das AVL apenas
com 2 índices, contudo verificamos que a procura era cerca de 0.4 segundos
mais demorada pelo que consideramos vantajosa a opção de manter o
endereçamento com 4 índices.

## Faturação

![Diagrama explicativo da Faturação](figures/faturacao.pdf){ height=10cm }

A faturação possui uma faturação anual e 12 faturações mensais. A faturação
anual é usada nas _queries_ em que são pedidos dados relativos a um ano,
enquanto que a faturação mensal é utilizada em _queries_ que são pedidos dados
relativos a um mês ou um intervalo finito de meses. Cada uma destas
faturações está organizada numa estrutura semelhante à dos catálogos de
clientes e produtos, sendo a justificação para utilização de tal estrutura
semelhante.

## Filiais

![Diagrama explicativo das Filiais](figures/filiais.pdf){ height=10cm }

A organização das filais é semelhante à da faturação, no entanto em cada nodo
da AVL está presente um Cliente, que contém um _ArrayList_ de _InfoProds_ que
estão ordenados alfabeticamente, mantendo assim, a procura eficiente.
