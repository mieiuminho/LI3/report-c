# Conclusão {#sec:end}

Os requisitos definidos para a aplicação `SGV` foram cumpridos na sua
totalidade. O produto final é uma aplicação de terminal que, após ler dados de
ficheiros de texto nos formatos especificados, é capaz de responder de forma
muito rápida às 12 _queries_.

Este projeto seguiu princípios de Engenharia de Software, tais como,
encapsulamento de dados, modularidade e reutilização de código. Tais
princípios foram expostos durante a unidade curricular (UC) de Laboratórios de
Informática III e consolidados durante o desenvolvimento deste programa.

A modularidade desta aplicação permitiria, com relativa facilidade, a introdução
de novas funcionalidades que a tornariam mais dinâmica. Estas passariam por
abrir a possibilidade de adicionar linhas de venda, por exemplo.

O sucesso deste projeto é o resultado da aquisição de conhecimentos
em UC's anteriores, nomeadamente, Laboratórios de Informática I e II,
Programação Funcional e Imperativa, aliada às novas técnicas introduzidas em
Laboratórios de Informática III.
