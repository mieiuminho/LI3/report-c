# Método de respostas às _queries_ {#sec:queries}

## _Query_ 1

São lidos os ficheiros de input fornecidos pelo utilizador (ou caso o utilizador
não forneça caminhos de ficheiros são lidos os ficheiros fornecidos pela equipa
docente) e inicializada a estrutura de dados `SGV`.

O ficheiro de input relativo aos produtos é utilizado para a construção do
catálogo de produtos e da faturação. Durante o processo de leitura é efetuada
a validação do código de produto e se esta for bem sucedida é criada uma
estrutura `Produto` e inserida no Catálogo de Produtos (`CatProdutos`). É
também criada uma estrutura `FacProd` e inserida faturação (`FacTotal`).

O ficheiro de input relativo aos clientes é utilizado para a construção do
catálogo de clientes e das filiais. Durante o processo de leitura é efetuada
a validação do código de cliente e se esta for bem sucedida é criada uma
estrutura `Cliente` e inserida no Catálogo de Clientes (`CatClientes`). É
também criada uma estrutura `ClienteProds` e inserida nas filiais.

O ficheiro de input relativo às vendas é utilizado para criar todas as
outras estruturas e atualizar as já existentes.

No caso do Catálogo de Produtos para cada produto é atualizado um _array_ de
`Bool` caso o produto tenha sido transacionado em alguma das filiais.

No caso do Catálogo de Clientes para cada cliente é atualizado um _array_ de
`Bool` caso o cliente tenha realizado compras em alguma das filiais.

Na faturação são atualizados os `FacProd` relativos a cada produto envolvido
numa linha de venda válida, mudando as quantidades (N ou P) e as faturações
(N ou P).

Nas filiais é criado ou atualizado um `InfoProd` para cada produto que
constar de uma linha de venda válida e que está associado a um cliente
mudando as quantidades (N ou P) e as faturações (N ou P).

## _Query_ 2

O método de reposta a esta _query_ é bastante simples, na medida em que apenas
é necessário aceder ao índice correspondente à letra decidida pelo utilizador.
Uma vez nesse índice temos uma _hashtable_ de AVL que contém todos os produtos
que pretendemos mostrar ao utilizador. Deste modo apenas temos que percorrer
todos os índices restantes (cada um contendo uma AVL) e transferir todos os
códigos de produto para um `ArrayList` que será fornecido ao navegador para ser
mostrado ao utilizador.

## _Query_ 3

Para responder a esta _query_ basta consultar a estrutura de dados faturação,
aceder a cada filial e devolver o `FacProd` correspondente ao produto escolhido
pelo utilizador. A resposta a esta _query_ é dada sob a forma de um _array_ de
`FacProd` de tamanho 3, cada _slot_ tem o `FacProd` correspondente ao produto,
no mês escolhido, em cada filial. No caso de o utilizador queres ter os dados na
sua forma condensada basta somar os todos os campos correspondentes de todas as
estruturas `FacProd` presentes no _array_.

## _Query_ 4

A resolução desta _query_ consiste em percorrer o catálogo de produtos e
verificar quais foram ou não comprados numa das filiais e ao mesmo
tempo quais deles não realizaram compras em nenhuma das filiais. Esta
verificação é feita consultando o _array_ de usados que existe em cada estrutura
do tipo `Produto`. A resposta à _query_ é dado sob a forma de um _array_ de
`ArrayList` com o tamanho de `NUM_FILIAIS + 1` na medida em que o último campo
do _array_ é utilizado para o `ArrayList` de clientes que não realizaram compras
em nenhuma das filiais.

## _Query_ 5

A resolução desta _query_ consiste em percorrer o catálogo de clientes e
verificar quais deles realizaram compras em todas filiais. Esta verificação é
feita consultando o _array_ de usados que existe em cada estrutura do tipo
`Cliente`. A resposta à _query_ é dada sob a forma de um _array_ de `ArrayList`
que contém os códigos de cliente dos clientes que respeitam a condição acima
descrita.

## _Query_ 6

O método de resolução desta _query_ é exatamente o mesmo das duas _queries_
anteriores, no entanto em vez de serem devolvidos `ArrayList` com os clientes
que não fizeram compras e com os produtos que não foram vendidos são devolvidos
inteiros com o número de clientes que não fizeram compras e com o número de
produtos que não foram vendidos.

## _Query_ 7

Nesta _query_ optamos por percorrer para todos os meses as filais mensais
(`FilialMensal`) de todas as filiais e obter a lista de `InfoProd` (produtos
comprados pelo cliente introduzido pelo utilizador num dado mês numa filial).
Seguidamente, iteramos essa lista e obtemos o número de unidades compradas
(somando as unidades adquiridas em regime N e o número de unidades compradas
em regime P).

## _Query_ 8

Na presente _query_ percorremos a estrutura da faturação mensal (`FacMensal`)
começando no índice inferior fornecido pelo utilizador e terminando no índice
superior também fornecido pelo utilizador. Na iteração referida, somamos os
campos `totalVendas` e `totalFacturado` de cada faturação mensal no intervalo
dado.

## _Query_ 9

O método de resposta a esta _query_ é: ter um _array_ de `ArrayList` de tamanho
dois (no primeiro índice ficam os clientes que efetuaram compras em regime N
e no segundo índice os clientes que efetuaram compras em regime P). Percorrer
todos os `ClienteProds` presentes na filial anual da filial pedida e verificar
e se realizaram compras em modo N e/ou P e acrescentar a um dos `ArrayList`
caso apenas realize compras num dos regimes ou aos dois `ArrayList` caso compre
produtos em modo N e P.

## _Query_ 10

Para responder a esta _query_ acedemos à filial anual da filial pedida e
copiamos o `ArrayList` de `InfoProds` do `ClienteProds` referente ao cliente
pedido e efetuamos em _QuickSort_ em ordem à quantidade comprada (em ordem
decrescente).

## _Query_ 11

Nesta _query_ optamos por começar por verificar os N produtos na `FacAnual` e
depois verificamos em todas as filiais, uma de cada vez, a sua filial anual
para verificar quantos `ClienteProds` (que se referem a um cliente) compraram
cada um dos N produtos mais vendidos verificados anteriormente. É devolvido
um _array_ de `ArrayList` com `NUM_FILIAIS` campos em que cada um contém o
código de cada um dos N produtos mais vendidos para cada filial e o número de
clientes envolvidos na compra do mesmo.

## _Query_ 12

Para responder a esta _query_ acedemos à filial anual de todas as filiais e
criamos um `ArrayList` de `InfoProds` referentes ao cliente pedido, sendo que
a cada filial: se o `InfoProd` não existir no `ArrayList` criado é inserido,
caso contrário atualizamos o valor previamente existente desse `InfoProd`.
Quando o `ArrayList` de `InfoProds` for definitivo, realizamos _QuickSort_ em
ordem à soma da faturação em ambos os regimes (por ordem decrescente) e
ficamos com os primeiros três.

